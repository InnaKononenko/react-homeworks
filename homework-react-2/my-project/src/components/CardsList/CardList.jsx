import React, { Component } from "react";
import PropTypes from "prop-types";
import Card from "./Card";

import "./cardList.scss";

export default class Cardlist extends Component {
  state = {
    selected: {},
  };

  selectedCard = (item) => {
    this.setState({
      selected: item,
    });
  };

  render() {
    const {
      goods,
      button,
      modal,
      showModal,
      closeModal,
      goodsInBasket,
      goodsFavorit,
      addProductToBasket,
      addProductToFavorites,
      deleteProductAtFavorites,
    } = this.props;

    return (
      <>
        {goods.length > 0 &&
          goods.map((item) => {
            const { name, price, url, article, color } = item;
            return (
              <Card
                addProductToFavorites={addProductToFavorites}
                addProductToBasket={addProductToBasket}
                selected={this.state.selected}
                selectedCard={this.selectedCard}
                key={article}
                item={item}
                goods={goods}
                name={name}
                price={price}
                url={url}
                article={article}
                color={color}
                button={button}
                modal={modal}
                showModal={showModal}
                closeModal={closeModal}
                goodsInBasket={goodsInBasket}
                goodsFavorit={goodsFavorit}
                deleteProductAtFavorites={deleteProductAtFavorites}
              />
            );
          })}
      </>
    );
  }
}
Cardlist.prorTypes = {
  goods: PropTypes.array,
  button: PropTypes.object,
  modal: PropTypes.object,
  showModal: PropTypes.func,
  closeModal: PropTypes.func,
  goodsInBasket: PropTypes.array,
  goodsFavorit: PropTypes.array,
  addProductToBasket: PropTypes.func,
  addProductToFavorites: PropTypes.func,
  deleteProductAtFavorites: PropTypes.func,
};
