import React, { Component } from "react";
import PropTypes from "prop-types";
import Button from "../../Buttons";
import Modal from "../../Modal";
import SecondaryButton from "../../SecondaryButton";
import FavoritesCard from "./FavoritesCard";

export default class Card extends Component {
  state = {
    actions: (
      <div className="btn-footer">
        <SecondaryButton
          text={"ok"}
          func={() => {
            this.props.addProductToBasket(this.props.selected);
          }}
        />
        <SecondaryButton text={"no"} func={() => this.props.closeModal()} />
      </div>
    ),
  };

  render() {
    const {
      selected,
      item,
      goods,
      name,
      price,
      url,
      article,
      color,
      button,
      modal,
      showModal,
      closeModal,
      goodsInBasket,
      goodsFavorit,
      selectedCard,
      addProductToBasket,
      addProductToFavorites,
      deleteProductAtFavorites,
    } = this.props;

    const { backgroundColor: bgcFirst, textOpen: textFirst } = button;
    const { actions } = this.state;
    const { show, header, text, closeButton } = modal;

    return (
      <div className="card" data-id={article}>
        <FavoritesCard
          className="card-favorites"
          goods={goodsFavorit}
          addProductToFavorites={addProductToFavorites}
          item={item}
          deleteProductAtFavorites={deleteProductAtFavorites}
        />
        <div className="card-body">
          <div className="card-header">
            <img className="card-img" src={url} alt="goods" />
            <p className="card-article">{article}</p>
          </div>
          <h5 className="card-name">
            {name.toUpperCase()} {color}
          </h5>
          <h3 className="card-price">{price}</h3>
        </div>
        <div className="button-container">
          <Button
            backgroundColor={bgcFirst}
            text={textFirst}
            onClick={() => {
              showModal();
              selectedCard(item);
            }}
          />
        </div>
        {show && (
          <Modal
            show={show}
            closeModal={closeModal}
            closeButton={closeButton}
            header={header}
            text={text}
            actions={actions}
          />
        )}
      </div>
    );
  }
}
Card.defaultProps = {
  article: "1010",
  color: "to choose",
};
Card.propTypes = {
  selected: PropTypes.object,
  item: PropTypes.object,
  goods: PropTypes.array,
  name: PropTypes.string,
  price: PropTypes.string,
  url: PropTypes.string,
  button: PropTypes.object,
  showModal: PropTypes.func,
  closeModal: PropTypes.func,
  goodsInBasket: PropTypes.array,
  goodsFavorit: PropTypes.array,
  selectedCard: PropTypes.func,
  addProductToBasket: PropTypes.func,
  addProductToFavorites: PropTypes.func,
};
