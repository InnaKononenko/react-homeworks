import React, { Component } from "react";
import PropTypes from "prop-types";
import { ReactComponent as Star } from "../img/star.svg";
import { ReactComponent as SelectFavorites } from "../../../Header/img/star-select.svg";

export default class FavoritesCard extends Component {
  state = {
    selectFavor: false,
    goodsFavorites: this.props.goods,
  };

  render() {
    const { goods, addProductToFavorites, item, deleteProductAtFavorites } =
      this.props;
    const { selectFavor, goodsFavorites } = this.state;
    return (
      <>
        {!selectFavor && (
          <Star
            onClick={() => {
              addProductToFavorites(item);
              this.setState({ ...this.state, selectFavor: true });
            }}
            className="card-favorites"
          />
        )}
        {selectFavor && (
          <SelectFavorites
            className="card-favorites"
            onClick={() => {
              deleteProductAtFavorites(item);
              this.setState({ ...this.state, selectFavor: false });
            }}
          />
        )}
        {goodsFavorites.map((product, index) => {
          if (product.article === item.article) {
            return (
              <SelectFavorites
                key={index}
                className="card-favorites"
                onClick={() => {
                  deleteProductAtFavorites(item);
                  this.setState({ ...this.state, selectFavor: false });
                }}
              />
            );
          }
        })}
      </>
    );
  }
}
FavoritesCard.propTypes = {
  goods: PropTypes.array,
  item: PropTypes.object,
  addProductToFavorites: PropTypes.func,
};
