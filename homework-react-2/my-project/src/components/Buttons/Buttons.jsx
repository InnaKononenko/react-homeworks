import React, { Component } from "react";
import PropTypes from "prop-types";
import "./buttons.scss";

export default class Button extends Component {
  render() {
    const { backgroundColor, text, onClick } = this.props;
    return (
      <button
        className="button"
        type="button"
        onClick={onClick}
        style={{ backgroundColor: backgroundColor }}
      >
        {text}
      </button>
    );
  }
}
Button.propTypes = {
  backgroundColor: PropTypes.string,
  text: PropTypes.string,
  onClick: PropTypes.func,
};
