import React, { Component } from "react";
import PropTypes from "prop-types";
import { ReactComponent as StarSvg } from "../img/star.svg";

export default class Favorites extends Component {
  addQuanity = () => {
    if (this.props.goodsFavorit.length > 0) {
      return (
        <h3 className="card-quantity-favorites">
          {this.props.goodsFavorit.length}
        </h3>
      );
    } else {
      return <h3 className="card-quantity-favorites">0</h3>;
    }
  };
  render() {
    const { goodsFavorit } = this.props;

    return (
      <>
        <StarSvg className="svg-star" />
        {this.addQuanity()}
      </>
    );
  }
}
Favorites.propTypes = {
  goodsFavorit: PropTypes.array,
};
