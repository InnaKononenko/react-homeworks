import React, { Component } from "react";
import Basket from "./Basket";
import Favorites from "./Favorites";
import PropTypes from "prop-types";
import "./header.scss";

export default class Header extends Component {
  render() {
    const { goods, goodsInBasket, goodsFavorit } = this.props;

    return (
      <div className="header">
        <div>
          <img className="logo" src="/shopogolik_logo.gif" alt="logo" />
        </div>
        <Basket goods={goods} goodsInBasket={goodsInBasket} />
        <Favorites goodsFavorit={goodsFavorit} />
      </div>
    );
  }
}
Header.propTypes = {
  goods: PropTypes.array,
  goodsInBasket: PropTypes.array,
  goodsFavorit: PropTypes.array,
};
