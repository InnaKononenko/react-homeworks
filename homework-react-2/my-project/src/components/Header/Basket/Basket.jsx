import React, { Component } from "react";
import PropTypes from "prop-types";
import { ReactComponent as BasketSvg } from "../img/basket.svg";

export default class Basket extends Component {
  addQuanity = () => {
    if (this.props.goodsInBasket.length > 0) {
      return (
        <h3 className="card-quantity">{this.props.goodsInBasket.length}</h3>
      );
    } else {
      return <h3 className="card-quantity">0</h3>;
    }
  };
  render() {
    const { goodsInBasket } = this.props;

    return (
      <>
        <BasketSvg className="svg-basket" />
        {this.addQuanity()}
      </>
    );
  }
}
Basket.propTypes = {
  goodsInBasket: PropTypes.array,
};
