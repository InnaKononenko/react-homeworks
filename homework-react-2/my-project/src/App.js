import React, { Component } from "react";
import SecondaryButton from "./components/SecondaryButton";

import "./App.css";
import Cardlist from "./components/CardsList/CardList";
import Header from "./components/Header/Header";

class App extends Component {
  state = {
    goods: [],
    goodsInBasket: [],
    goodsFavorit: [],
    button: {
      backgroundColor: "rgb(247, 228, 247)",
      textOpen: "Add to cart",
    },
    modal: {
      show: false,
      text: "Add to cart?",
      header: (
        <div>
          <img className="logo-modal" src="/shopogolik_logo.gif" alt="logo" />
        </div>
      ),
      closeButton: true,
      backgroundColor: null,
      actions: (
        <div className="btn-footer">
          <SecondaryButton text={"ok"} func={() => {}} />
          <SecondaryButton text={"no"} func={() => this.closeModal()} />
        </div>
      ),
    },
  };

  showModal = () => {
    const { modal } = this.state;
    this.setState({ ...this.state, modal: { ...modal, show: true } });
  };
  closeModal = () => {
    const { modal } = this.state;
    this.setState({
      ...this.state,
      modal: { ...modal, show: false },
    });
  };

  addProductToBasket = (product) => {
    const { modal } = this.state;

    this.setState((prevState) => ({
      goodsInBasket: [...prevState.goodsInBasket, product],
      modal: { ...modal, show: false },
    }));
  };

  addProductToFavorites = (product) => {
    this.setState((prevState) => ({
      goodsFavorit: [...prevState.goodsFavorit, product],
    }));
  };
  deleteProductAtFavorites = (product) => {
    this.setState((prevState) => ({
      goodsFavorit: prevState.goodsFavorit.filter(
        (el) => el.article !== product.article
      ),
    }));
  };
  componentDidUpdate() {
    localStorage.setItem(
      "goodsInBasket",
      JSON.stringify(this.state.goodsInBasket)
    );
    localStorage.setItem(
      "goodsFavorit",
      JSON.stringify(this.state.goodsFavorit)
    );
  }
  componentDidMount() {
    fetch(`./goodsList/goodsList.json`, {
      headers: {
        "Content-Type": "application/json",
        Accept: "application/json",
      },
    })
      .then((res) => res.json())
      .then((data) =>
        this.setState({
          ...this.state,
          goods: data,
        })
      );
    if (
      localStorage.getItem("goodsInBasket") ||
      localStorage.getItem("goodsFavorit")
    ) {
      const parseBasket = JSON.parse(localStorage.getItem("goodsInBasket"));
      const parseFavorite = JSON.parse(localStorage.getItem("goodsFavorit"));
      this.setState({
        ...this.state,
        goodsInBasket: parseBasket,
        goodsFavorit: parseFavorite,
      });
    } else {
      localStorage.setItem(
        "goodsInBasket",
        JSON.stringify(this.state.goodsInBasket)
      );
      localStorage.setItem(
        "goodsFavorit",
        JSON.stringify(this.state.goodsFavorit)
      );
    }
  }

  render() {
    const { goods, button, modal, goodsInBasket, goodsFavorit } = this.state;

    return (
      <>
        <Header
          goods={goods}
          goodsInBasket={goodsInBasket}
          goodsFavorit={goodsFavorit}
        />
        <div className="container">
          <div className="card-list">
            <Cardlist
              addProductToBasket={this.addProductToBasket}
              addProductToFavorites={this.addProductToFavorites}
              goods={goods}
              button={button}
              modal={modal}
              showModal={this.showModal}
              closeModal={this.closeModal}
              goodsInBasket={goodsInBasket}
              goodsFavorit={goodsFavorit}
              deleteProductAtFavorites={this.deleteProductAtFavorites}
            />
          </div>
        </div>
      </>
    );
  }
}

export default App;
