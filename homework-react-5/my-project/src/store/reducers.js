import { createReducer } from "@reduxjs/toolkit";

import * as actions from "./actions";

const defaultState = {
  formData: {
    name: "",
    lastName: "",
    age: "",
    address: "",
    phone: "",
  },
  goods: [],
  goodsBasket: [],
  goodsFavorit: [],
  modal: {
    show: false,
    text: "",
    header: (
      <div>
        <img className="logo-modal" src="/shopogolik_logo.gif" alt="logo" />
      </div>
    ),
    closeButton: true,
    backgroundColor: null,
    actions: "",
  },
};

export default createReducer(defaultState, {
  [actions.setGoods]: (state, { payload }) => {
    state.goods = payload;
  },

  [actions.addGoodsInBasket]: (state, { payload }) => {
    state.goodsBasket = [...state.goodsBasket, payload];
  },
  [actions.addGoodsBasketInLokal]: (state, { payload }) => {
    state.goodsBasket = payload;
  },
  [actions.setGoodsFavorit]: (state, { payload }) => {
    state.goodsFavorit = [...state.goodsFavorit, payload];
  },
  [actions.addGoodsFavoritInLokal]: (state, { payload }) => {
    state.goodsFavorit = payload;
  },
  [actions.deleteGoodsFromBasket]: (state, { payload }) => {
    state.goodsBasket = payload;
  },
  [actions.deleteGoodsFromFavorit]: (state, { payload }) => {
    state.goodsFavorit = payload;
  },
  [actions.setModal]: (state, { payload }) => {
    state.modal.show = payload;
  },
  [actions.setFormData]: (state, { payload }) => {
    state.formData = payload;
  },
  [actions.cleanGoodsBasket]: (state) => {
    state.goodsBasket = [];
  },
});
