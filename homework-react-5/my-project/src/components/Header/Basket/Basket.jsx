import React from "react";
import PropTypes from "prop-types";
import Cardlist from "../../CardsList/CardList";
import { useSelector } from "react-redux";

import { selectorGoodsBasket } from "../../../store/selectors";
import ComeBackPage from "../../ComeBackPage";
import PageForm from "../../Forms/PageForm/PageForm";
import { NavLink } from "react-router-dom";
import Button from "../../Buttons";

const Basket = (props) => {
  const goodsInBasket = useSelector(selectorGoodsBasket);

  const { showModal, closeModal, isBtnDisabled, basketPage } = props;

  return (
    <>
      <ComeBackPage title="back to shopping" />
      <Cardlist
        goods={goodsInBasket}
        showModal={showModal}
        closeModal={closeModal}
        isBtnDisabled={isBtnDisabled}
        basketPage={basketPage}
        text={"Delete from card?"}
      />
      {goodsInBasket.length === 0 && (
        <h1 className="empty-string">Your shopping cart is empty:(</h1>
      )}
      {goodsInBasket.length > 0 && (
        <NavLink to="/pageForm">
          <div className="btn-order">
            <Button
              className="button_order"
              backgroundColor={"rgb(125, 110, 96)"}
              text={"To order"}
            />
          </div>
        </NavLink>
      )}
    </>
  );
};
Basket.propTypes = {
  goodsInBasket: PropTypes.array,
};
export default Basket;
