import * as yup from "yup";

export const validation = yup.object().shape({
  name: yup.string("Enter your name").required("Name is required"),
  lastName: yup
    .string("Enter your last name")
    .required("Last name is required"),
  age: yup.number(),
  address: yup.string().required("Address is required"),
  phone: yup.number().required("Phone is required"),
});
