import React from "react";
import PropTypes from "prop-types";
import "./buttons.scss";

const Buttons = ({
  className = "button",
  backgroundColor,
  text,
  onClick,
  isBtnDisabled = false,
  type = "button",
}) => {
  return (
    <button
      className={className}
      type={type}
      onClick={onClick}
      style={{
        backgroundColor: backgroundColor,
        display: isBtnDisabled && "none",
      }}
    >
      {text}
    </button>
  );
};

Buttons.propTypes = {
  backgroundColor: PropTypes.string,
  text: PropTypes.string,
  onClick: PropTypes.func,
};
export default Buttons;
