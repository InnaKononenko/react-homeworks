import React from "react";
import { createBrowserHistory } from "history";
import { ReactComponent as LeftArrow } from "./images/left_arrow.svg";
import { useLocation } from "react-router-dom";

import "./ComeBackPage.scss";

const HeaderPage = ({ title }) => {
  const history = createBrowserHistory({ window });
  const { key } = useLocation();
  const clickBack = () => {
    history.back();
  };

  return (
    <div className="dashboard__header">
      {key !== "default" && (
        <span className="btn-back" onClick={clickBack}>
          <LeftArrow />
        </span>
      )}
      {title}
    </div>
  );
};

export default HeaderPage;
