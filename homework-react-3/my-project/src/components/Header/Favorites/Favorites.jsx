import React from "react";
import PropTypes from "prop-types";
import Cardlist from "../../CardsList/CardList";
import ComeBackPage from "../../ComeBackPage";
const Favorites = (props) => {
  const {
    button,
    goodsFavorit,
    addProductToBasket,
    addProductToFavorites,
    modal,
    showModal,
    closeModal,
    deleteProductAtFavorites,
    textOpen,
    text,
  } = props;

  return (
    <>
      <ComeBackPage title="back to shopping" />
      <Cardlist
        addProductToFavorites={addProductToFavorites}
        goods={goodsFavorit}
        button={button}
        modal={modal}
        showModal={showModal}
        closeModal={closeModal}
        goodsFavorit={goodsFavorit}
        deleteProductAtFavorites={deleteProductAtFavorites}
        textOpen={textOpen}
        text={text}
        addProductToBasket={addProductToBasket}
      />
      {goodsFavorit.length === 0 && (
        <h1 className="empty-string">You haven't liked anything yet:(</h1>
      )}
    </>
  );
};
Favorites.propTypes = {
  goodsFavorit: PropTypes.array,
};
export default Favorites;
