import React from "react";
import PropTypes from "prop-types";
import Cardlist from "../../CardsList/CardList";
import ComeBackPage from "../../ComeBackPage";
const Basket = (props) => {
  const {
    goodsInBasket,
    goods,
    goodsFavorit,
    addProductToBasket,
    addProductToFavorites,
    button,
    modal,
    showModal,
    closeModal,
    deleteProductAtFavorites,
    isBtnDisabled,
    basketPage,
    text,
    deleteFromBasket,
  } = props;

  return (
    <>
      <ComeBackPage title="back to shopping" />
      <Cardlist
        addProductToFavorites={addProductToFavorites}
        goods={goodsInBasket}
        button={button}
        modal={modal}
        showModal={showModal}
        closeModal={closeModal}
        goodsFavorit={goodsFavorit}
        deleteProductAtFavorites={deleteProductAtFavorites}
        isBtnDisabled={isBtnDisabled}
        basketPage={basketPage}
        text={text}
        deleteFromBasket={deleteFromBasket}
      />
      {goodsInBasket.length === 0 && (
        <h1 className="empty-string">Your shopping cart is empty:(</h1>
      )}
    </>
  );
};
Basket.propTypes = {
  goodsInBasket: PropTypes.array,
};
export default Basket;
