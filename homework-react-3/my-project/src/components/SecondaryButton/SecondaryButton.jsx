import React from "react";
import PropTypes from "prop-types";

import "./secondaryButton.scss";

const SecondaryButton = ({ text, func }) => {
  return (
    <>
      <button className="btn-text" type="button" onClick={func}>
        {text}
      </button>
    </>
  );
};
SecondaryButton.propTypes = {
  text: PropTypes.string,
  func: PropTypes.func,
};
export default SecondaryButton;
