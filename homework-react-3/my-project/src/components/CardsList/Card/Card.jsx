import React, { useEffect, useState } from "react";
import PropTypes from "prop-types";
import Button from "../../Buttons";
import Modal from "../../Modal";
import SecondaryButton from "../../SecondaryButton";
import FavoritesCard from "./FavoritesCard";

const Card = (props) => {
  const {
    selected,
    selectedCard,
    item,
    goods,
    button,
    modal,
    showModal,
    closeModal,
    goodsInBasket,
    goodsFavorit,
    addProductToBasket,
    addProductToFavorites,
    deleteProductAtFavorites,
    text,
    textOpen,
    backgroundColor,
    isBtnDisabled,
    basketPage = false,
    deleteFromBasket,
    setButton,
  } = props;

  const { show, header, closeButton } = modal;
  const { name, price, url, article, color } = item;

  return (
    <div className="card">
      <FavoritesCard
        className="card-favorites"
        goodsFavorit={goodsFavorit}
        addProductToFavorites={addProductToFavorites}
        item={item}
        deleteProductAtFavorites={deleteProductAtFavorites}
      />
      {basketPage && (
        <button
          type="button"
          className="delete-card"
          onClick={() => {
            showModal();
            selectedCard(item);
          }}
        >
          <span>&times;</span>
        </button>
      )}
      <div className="card-body">
        <div className="card-header">
          <img className="card-img" src={url} alt="goods" />
          <p className="card-article">{article}</p>
        </div>
        <h5 className="card-name">
          {name.toUpperCase()} {color}
        </h5>
        <h3 className="card-price">{price}</h3>
      </div>
      <div className="button-container">
        <Button
          key={article}
          backgroundColor={backgroundColor}
          text={textOpen}
          isBtnDisabled={isBtnDisabled}
          onClick={() => {
            showModal();
            selectedCard(item);
          }}
        />
      </div>
      {show && (
        <Modal
          show={show}
          closeModal={closeModal}
          closeButton={closeButton}
          header={header}
          text={text}
          actions={
            <div className="btn-footer">
              {!basketPage && (
                <SecondaryButton
                  text={"ok"}
                  func={() => {
                    addProductToBasket(selected);
                  }}
                />
              )}

              {basketPage && (
                <SecondaryButton
                  text={"ok"}
                  func={() => {
                    deleteFromBasket(selected);
                  }}
                />
              )}
              <SecondaryButton text={"no"} func={() => closeModal()} />
            </div>
          }
        />
      )}
    </div>
  );
};

Card.defaultProps = {
  article: "1010",
  color: "to choose",
};
Card.propTypes = {
  selected: PropTypes.object,
  item: PropTypes.object,
  goods: PropTypes.array,
  name: PropTypes.string,
  price: PropTypes.string,
  url: PropTypes.string,
  button: PropTypes.object,
  showModal: PropTypes.func,
  closeModal: PropTypes.func,
  goodsInBasket: PropTypes.array,
  goodsFavorit: PropTypes.array,
  selectedCard: PropTypes.func,
  addProductToBasket: PropTypes.func,
  addProductToFavorites: PropTypes.func,
};
export default Card;
