import React, { useState } from "react";
import PropTypes from "prop-types";
import { ReactComponent as Star } from "../img/star.svg";
import { ReactComponent as SelectFavorites } from "../../../Header/img/star-select.svg";

const FavoritesCard = (props) => {
  const {
    goodsFavorit,
    addProductToFavorites,
    item,
    deleteProductAtFavorites,
  } = props;
  const [selectFavor, setSelectFavor] = useState(false);

  return (
    <>
      {!selectFavor && (
        <Star
          onClick={() => {
            addProductToFavorites(item);
            setSelectFavor(true);
          }}
          className="card-favorites"
        />
      )}
      {selectFavor && (
        <SelectFavorites
          className="card-favorites"
          onClick={() => {
            deleteProductAtFavorites(item);
            setSelectFavor(false);
          }}
        />
      )}
      {goodsFavorit.map((product, index) => {
        if (product.article === item.article) {
          return (
            <SelectFavorites
              key={index}
              className="card-favorites"
              onClick={() => {
                deleteProductAtFavorites(item);
                setSelectFavor(false);
              }}
            />
          );
        }
      })}
    </>
  );
};

FavoritesCard.propTypes = {
  goods: PropTypes.array,
  item: PropTypes.object,
  addProductToFavorites: PropTypes.func,
};
export default FavoritesCard;
