import React, { useState } from "react";
import PropTypes from "prop-types";
import Card from "./Card";

import "./cardList.scss";

const Cardlist = (props) => {
  const {
    goods,
    button,
    modal,
    showModal,
    closeModal,
    goodsInBasket,
    goodsFavorit,
    addProductToBasket,
    addProductToFavorites,
    deleteProductAtFavorites,
    text,
    backgroundColor,
    textOpen,
    isBtnDisabled,
    basketPage,
    deleteFromBasket,
    setButton,
  } = props;
  const [selected, setSelected] = useState({});

  const selectedCard = (item) => {
    setSelected(item);
  };

  return (
    <div className="card-list">
      {goods.length > 0 &&
        goods.map((item, key) => {
          return (
            <Card
              setButton={setButton}
              text={text}
              addProductToFavorites={addProductToFavorites}
              addProductToBasket={addProductToBasket}
              selected={selected}
              selectedCard={selectedCard}
              key={key}
              item={item}
              goods={goods}
              button={button}
              backgroundColor={backgroundColor}
              textOpen={textOpen}
              modal={modal}
              showModal={showModal}
              closeModal={closeModal}
              goodsInBasket={goodsInBasket}
              goodsFavorit={goodsFavorit}
              deleteProductAtFavorites={deleteProductAtFavorites}
              isBtnDisabled={isBtnDisabled}
              basketPage={basketPage}
              deleteFromBasket={deleteFromBasket}
            />
          );
        })}
    </div>
  );
};

Cardlist.prorTypes = {
  goods: PropTypes.array,
  button: PropTypes.object,
  modal: PropTypes.object,
  showModal: PropTypes.func,
  closeModal: PropTypes.func,
  goodsInBasket: PropTypes.array,
  goodsFavorit: PropTypes.array,
  addProductToBasket: PropTypes.func,
  addProductToFavorites: PropTypes.func,
  deleteProductAtFavorites: PropTypes.func,
};
export default Cardlist;
