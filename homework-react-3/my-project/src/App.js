import React, { useEffect, useState } from "react";
import { Route, Routes } from "react-router-dom";
import "./App.css";
import Cardlist from "./components/CardsList/CardList";
import Header from "./components/Header/Header";
import Basket from "./components/Header/Basket";
import Favorites from "./components/Header/Favorites";
import Footer from "./components/Footer";

const App = () => {
  const [goods, setGoods] = useState([]);
  const [goodsInBasket, setGoodsInBasket] = useState([]);
  const [goodsFavorit, setGoodsFavorit] = useState([]);
  const [button, setButton] = useState({
    backgroundColor: "",
    textOpen: "",
  });

  const [modal, setModal] = useState({
    show: false,
    text: "",
    header: (
      <div>
        <img className="logo-modal" src="/shopogolik_logo.gif" alt="logo" />
      </div>
    ),
    closeButton: true,
    backgroundColor: null,
    actions: "",
  });

  useEffect(() => {
    fetch(`./goodsList/goodsList.json`, {
      headers: {
        "Content-Type": "application/json",
        Accept: "application/json",
      },
    })
      .then((res) => res.json())
      .then((data) => setGoods(data));
  }, []);

  useEffect(() => {
    if (localStorage.getItem("goodsInBasket")) {
      const parseBasket = JSON.parse(localStorage.getItem("goodsInBasket"));

      setGoodsInBasket(parseBasket);
    } else {
      localStorage.setItem("goodsInBasket", JSON.stringify(goodsInBasket));
    }

    if (localStorage.getItem("goodsFavorit")) {
      const parseFavorite = JSON.parse(localStorage.getItem("goodsFavorit"));
      setGoodsFavorit(parseFavorite);
    } else {
      localStorage.setItem("goodsFavorit", JSON.stringify(goodsFavorit));
    }
  }, []);

  useEffect(() => {
    localStorage.setItem("goodsInBasket", JSON.stringify(goodsInBasket));
    localStorage.setItem("goodsFavorit", JSON.stringify(goodsFavorit));
  }, [goodsInBasket, goodsFavorit]);

  const showModal = () => {
    setModal({ ...modal, show: true });
  };

  const closeModal = () => {
    setModal({ ...modal, show: false });
  };

  const addProductToBasket = (product) => {
    const arrArticle = [];
    goodsInBasket.length > 0 &&
      goodsInBasket.map((el) => {
        arrArticle.push(el.article);

        if (arrArticle.includes(product.article)) {
          setGoodsInBasket([...goodsInBasket]);
        } else {
          setGoodsInBasket([...goodsInBasket, product]);
        }
      });
    goodsInBasket.length === 0 && setGoodsInBasket([...goodsInBasket, product]);
    setModal({ ...modal, show: false });
  };

  const addProductToFavorites = (product) => {
    setGoodsFavorit([...goodsFavorit, product]);
  };

  const deleteProductAtFavorites = (product) => {
    setGoodsFavorit(
      goodsFavorit.filter((el) => el.article !== product.article)
    );
  };
  const deleteFromBasket = (product) => {
    setGoodsInBasket(
      goodsInBasket.filter((el) => el.article !== product.article)
    );
    setModal({ ...modal, show: false });
  };

  return (
    <>
      <Header
        goods={goods}
        goodsInBasket={goodsInBasket}
        goodsFavorit={goodsFavorit}
        addProductToBasket={addProductToBasket}
        addProductToFavorites={addProductToFavorites}
        button={button}
        modal={modal}
        showModal={showModal}
        closeModal={closeModal}
        deleteProductAtFavorites={deleteProductAtFavorites}
      />
      <div className="container">
        <Routes>
          <Route
            path="/basket"
            element={
              <Basket
                backgroundColor={"rgb(247, 228, 247)"}
                textOpen="Add to cart"
                isBtnDisabled={true}
                modal={modal}
                goodsInBasket={goodsInBasket}
                addProductToBasket={addProductToBasket}
                addProductToFavorites={addProductToFavorites}
                goods={goods}
                showModal={showModal}
                closeModal={closeModal}
                goodsFavorit={goodsFavorit}
                deleteProductAtFavorites={deleteProductAtFavorites}
                basketPage={true}
                text={"Delete from card?"}
                deleteFromBasket={deleteFromBasket}
              />
            }
          />
          <Route
            path="/favorites"
            element={
              <Favorites
                backgroundColor={"rgb(247, 228, 247)"}
                textOpen={"Add to card?"}
                text={"Add to card?"}
                isBtnDisabled={true}
                modal={modal}
                goodsInBasket={goodsInBasket}
                addProductToBasket={addProductToBasket}
                addProductToFavorites={addProductToFavorites}
                goods={goods}
                showModal={showModal}
                closeModal={closeModal}
                goodsFavorit={goodsFavorit}
                deleteProductAtFavorites={deleteProductAtFavorites}
                button={button}
              />
            }
          />
          <Route
            path="/"
            element={
              <Cardlist
                addProductToBasket={addProductToBasket}
                addProductToFavorites={addProductToFavorites}
                goods={goods}
                button={button}
                setButton={setButton}
                backgroundColor={"rgb(247, 228, 247)"}
                textOpen="Add to cart"
                modal={modal}
                text={"Add to card?"}
                setModal={setModal}
                showModal={showModal}
                closeModal={closeModal}
                goodsInBasket={goodsInBasket}
                goodsFavorit={goodsFavorit}
                deleteProductAtFavorites={deleteProductAtFavorites}
              />
            }
          />
        </Routes>
      </div>
      <Footer />
    </>
  );
};
export default App;
