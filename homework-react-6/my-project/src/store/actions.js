import { createAction } from "@reduxjs/toolkit";

export const setGoods = createAction("SET_GOODS");
export const addGoodsInBasket = createAction("ADD_GOODS_IN_BASKET");
export const setGoodsFavorit = createAction("SET_GOODS_FAVORIT");
export const setModal = createAction("SET_MODAL");
export const addGoodsBasketInLokal = createAction("ADD_GOODS_BASKET_IN_LOKAL");
export const addGoodsFavoritInLokal = createAction(
  "ADD_GOODS_FAVORIT_IN_LOKAL"
);
export const deleteGoodsFromBasket = createAction("DELETE_GOODS_FROM_BASKET");
export const deleteGoodsFromFavorit = createAction("DELETE_GOODS_FROM_FAVORIT");

export const fetchGoodsList = () => (dispatch) => {
  return fetch(`./goodsList/goodsList.json`, {
    headers: {
      "Content-Type": "application/json",
      Accept: "application/json",
    },
  })
    .then((res) => res.json())
    .then((data) => {
      dispatch(setGoods(data));
    });
};
export const setFormData = createAction("SET_FORM_DATA");

export const cleanGoodsBasket = createAction("CLEAN_GOODS_BASKET");
