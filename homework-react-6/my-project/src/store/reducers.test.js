import reducers from "./reducers";
import { defaultState } from "./reducers";

describe("post reducer", () => {
  it("should return the initial state", () => {
    expect(reducers(undefined, {})).toEqual(defaultState);
  });
});
