import { render, screen } from "@testing-library/react";

import { BrowserRouter } from "react-router-dom";
import Modal from "./Modal";
import { Provider } from "react-redux";
import store from "../../store/store";

describe("<Modal/> component", () => {
  it("find modal title", () => {
    render(
      <Provider store={store}>
        <Modal />
        Add to card
      </Provider>
    );

    screen.getByText(/Add to card/);
  });
});
