import { render, screen } from "@testing-library/react";
import Buttons from "./Buttons";
import { BrowserRouter } from "react-router-dom";

describe("<Buttons/> component", () => {
  const { container } = render(<Buttons />);
  it("should render", () => {
    // eslint-disable-next-line testing-library/no-node-access
    expect(container.firstChild).toMatchSnapshot();
  });

  it("should render button with text", () => {
    render(<Buttons text="Delete" />);
    screen.getByText(/Delete/i);
  });
});
