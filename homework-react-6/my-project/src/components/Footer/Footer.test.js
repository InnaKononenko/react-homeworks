import { render, screen } from "@testing-library/react";

import Footer from "./Footer";

describe("<Footer/> component", () => {
  const { container } = render(<Footer />);
  it("should render", () => {
    // eslint-disable-next-line testing-library/no-node-access
    expect(container.firstChild).toMatchSnapshot();
  });
});
