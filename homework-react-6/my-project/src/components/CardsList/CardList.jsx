import React, { useState, useContext } from "react";
import PropTypes from "prop-types";
import Card from "./Card";
import cx from "classnames";
import { TypeContext } from "../../context";
import "./cardList.scss";
import { ReactComponent as ListSvg } from "./img/format-list.svg";
import { ReactComponent as CardSvg } from "./img/format-card.svg";

const Cardlist = (props) => {
  const { type, changeType } = useContext(TypeContext);

  const { goods, showModal, closeModal, isBtnDisabled, basketPage, text } =
    props;
  const [selected, setSelected] = useState({});

  const selectedCard = (item) => {
    setSelected(item);
  };

  return (
    <>
      <div className="display-format">
        {type && goods.length > 0 && (
          <ListSvg
            onClick={() => {
              changeType();
            }}
          />
        )}

        {!type && goods.length > 0 && (
          <CardSvg
            onClick={() => {
              changeType();
            }}
          />
        )}
      </div>
      <div className={cx("card-list", { "type-list": !type })}>
        {goods.length > 0 &&
          goods.map((item, key) => {
            return (
              <Card
                selected={selected}
                selectedCard={selectedCard}
                key={key}
                item={item}
                goods={goods}
                showModal={showModal}
                closeModal={closeModal}
                isBtnDisabled={isBtnDisabled}
                basketPage={basketPage}
                text={text}
              />
            );
          })}
      </div>
    </>
  );
};

Cardlist.prorTypes = {
  selected: PropTypes.object,
  selectedCard: PropTypes.func,
  text: PropTypes.string,
  goods: PropTypes.array,
  button: PropTypes.object,
  showModal: PropTypes.func,
  closeModal: PropTypes.func,
};
export default Cardlist;
