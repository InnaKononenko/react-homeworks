import React from "react";
import { Field, ErrorMessage } from "formik";
import PropTypes from "prop-types";
const Input = ({ type, placeholder, label, name, className, error }) => {
  return (
    <label className={className}>
      <p className="form-label">{label}</p>
      <Field type={type} name={name} placeholder={placeholder} />
      <ErrorMessage name={name} className="error-massage" component={"p"} />
    </label>
  );
};
// Input.defaultProps = {
//   type: "text",
// };
// Input.propTypes = {
//   type: PropTypes.string,
// };
export default Input;
