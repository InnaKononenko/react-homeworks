import React from "react";
import { Formik, useFormik } from "formik";
import { useSelector, useDispatch } from "react-redux";
import {
  selectorFormData,
  selectorGoodsBasket,
} from "../../../store/selectors";
import { setFormData, cleanGoodsBasket } from "../../../store/actions";
import { Input } from "../";
import Button from "../../Buttons";
import "./PageForm.scss";
import { createBrowserHistory } from "history";
import { validation } from "./validation";
import { NavLink } from "react-router-dom";

const PageForm = () => {
  const history = createBrowserHistory({ window });
  const dispatch = useDispatch();
  const formData = useSelector(selectorFormData);
  const goodsInBasket = useSelector(selectorGoodsBasket);
  let names = "";
  goodsInBasket.forEach((el) => {
    names += el.name + ";";
  });

  return (
    <div className="form-wrapper">
      <Formik
        initialValues={formData}
        onSubmit={(values) => {
          console.log(
            "%c%s%c %c%s",
            "background:orange",
            "Congratulations! You bought:",
            "background:inherit;",
            "background:yellow;",
            names
          );
          console.log(
            "%c%s%c ",
            "background:orange",
            "Delivery data",
            "background:inherit;",
            "Name:",
            values.name + " " + values.lastName,
            "Address:",
            values.address,
            "Phone:",
            values.phone
          );

          dispatch(setFormData(values));
          dispatch(cleanGoodsBasket());
          history.back();
        }}
        validationSchema={validation}
      >
        {({ errors, touched, getFieldProps, handleSubmit }) => (
          <form onSubmit={handleSubmit} className="form">
            <Input
              name={"name"}
              placeholder="name"
              className={"form-item"}
              label={"Name"}
              error={errors.name && touched.name}
            />
            <Input
              name={"lastName"}
              placeholder="last name"
              className={"form-item"}
              label={"Last name"}
              error={errors.lastName && touched.lastName}
            />
            <Input
              name={"age"}
              placeholder="age"
              className={"form-item"}
              label={"Age"}
              error={errors.age && touched.age}
            />

            <Input
              name={"phone"}
              placeholder="phone"
              className={"form-item"}
              label={"Phone"}
              error={errors.phone && touched.phone}
            />
            <Input
              name={"address"}
              placeholder="address"
              className={"form-item"}
              label={"Address"}
              error={errors.address && touched.address}
            />
            <div className="btn-order">
              <Button
                type="submit"
                backgroundColor={"rgb(125, 110, 96)"}
                text={"Сonfirm purchase"}
              />
            </div>
          </form>
        )}
      </Formik>
    </div>
  );
};

export default PageForm;
