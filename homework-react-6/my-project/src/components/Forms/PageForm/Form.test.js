import { render, screen } from "@testing-library/react";
import { BrowserRouter } from "react-router-dom";
import "@testing-library/jest-dom";
import PageForm from "./PageForm";
import { Provider } from "react-redux";
import store from "../../../store/store";
import { Formik } from "formik";
import Input from "../Input";

describe("PageForm component", () => {
  const { container } = render(
    <Provider store={store}>
      <PageForm />
    </Provider>
  );
  it("find input label text", () => {
    const { getByLabelText } = render(
      <Formik>
        <Input name="name" label={"text"} />
      </Formik>
    );
    // eslint-disable-next-line testing-library/prefer-screen-queries
    expect(getByLabelText("text")).toBeInTheDocument();
  });

  it("should render", () => {
    // eslint-disable-next-line testing-library/no-node-access
    expect(container.firstChild).toMatchSnapshot();
  });
});
