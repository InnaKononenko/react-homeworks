import { createContext, useState } from "react";

export const TypeContext = createContext({});

const TypeProvider = ({ children }) => {
  let [type, setType] = useState(true);

  const changeType = () => {
    type = !type;
    setType(type);
  };
  return (
    <TypeContext.Provider value={{ type, changeType }}>
      {children}
    </TypeContext.Provider>
  );
};
export default TypeProvider;
