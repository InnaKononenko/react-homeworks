import { createStore, applyMiddleware } from "redux";
import { composeWithDevTools } from "redux-devtools-extension";
import thunk from "redux-thunk";
import logger from "redux-logger";

import rootReducers from "./reducers";

// const devTools = window.__REDUX_DEVTOOLS_EXTENSION__
//   ? window.__REDUX_DEVTOOLS_EXTENSION__()
//   : (f) => f;

const store = createStore(
  rootReducers,
  composeWithDevTools(applyMiddleware(thunk, logger))
);

export default store;
