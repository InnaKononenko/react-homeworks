export const selectorGoods = (state) => state.goods;
export const selectorGoodsBasket = (state) => state.goodsBasket;
export const selectorGoodsFavorit = (state) => state.goodsFavorit;
export const selectorModal = (state) => state.modal;
