import React from "react";
import PropTypes from "prop-types";
import "./modal.scss";

const Modal = (props) => {
  const { show, closeModal, header, text, closeButton, actions } = props;

  return (
    show && (
      <div className="modal" onClick={closeModal}>
        <div
          className="modal-content"
          onClick={(e) => {
            e.stopPropagation();
          }}
        >
          <div className="modal-header">
            <h5 className="modal-title">{header}</h5>
            {!!closeButton && (
              <button type="button" className="close" onClick={closeModal}>
                <span>&times;</span>
              </button>
            )}
          </div>
          <div className="modal-body">
            <h3>{text}</h3>
          </div>
          <div className="modal-footer">{actions}</div>
        </div>
      </div>
    )
  );
};

Modal.defaultProps = {
  header: "select",
};
Modal.propTypes = {
  show: PropTypes.bool,
  closeModal: PropTypes.func,
  text: PropTypes.string,
  closeButton: PropTypes.bool,
  actions: PropTypes.object,
};
export default Modal;
