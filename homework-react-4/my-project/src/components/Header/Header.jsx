import React, { Component } from "react";

import PropTypes from "prop-types";
import "./header.scss";
import { NavLink, Link } from "react-router-dom";

import { ReactComponent as BasketSvg } from "./img/basket.svg";
import { ReactComponent as StarSvg } from "./img/star.svg";
import { useSelector } from "react-redux";

import {
  selectorGoodsBasket,
  selectorGoodsFavorit,
} from "../../store/selectors";
const Header = () => {
  const goodsInBasket = useSelector(selectorGoodsBasket);
  const goodsFavorit = useSelector(selectorGoodsFavorit);

  const addQuanity = (arr, className) => {
    if (arr.length > 0) {
      return <h3 className={className}>{arr.length}</h3>;
    } else {
      return <h3 className={className}>0</h3>;
    }
  };

  return (
    <div className="header">
      <div>
        <Link to="/">
          <img className="logo" src="/shopogolik_logo.gif" alt="logo" />
        </Link>
      </div>
      <NavLink to="/basket">
        <BasketSvg className="svg-basket" />
        {addQuanity(goodsInBasket, "card-quantity")}
      </NavLink>
      <NavLink to="/favorites">
        <StarSvg className="svg-star" />
        {addQuanity(goodsFavorit, "card-quantity-favorites")}
      </NavLink>
    </div>
  );
};

Header.propTypes = {
  goods: PropTypes.array,
  goodsInBasket: PropTypes.array,
  goodsFavorit: PropTypes.array,
};
export default Header;
