import React from "react";
import PropTypes from "prop-types";
import Cardlist from "../../CardsList/CardList";
import { useSelector } from "react-redux";

import { selectorGoodsBasket } from "../../../store/selectors";
import ComeBackPage from "../../ComeBackPage";

const Basket = (props) => {
  const goodsInBasket = useSelector(selectorGoodsBasket);

  const { showModal, closeModal, isBtnDisabled, basketPage } = props;

  return (
    <>
      <ComeBackPage title="back to shopping" />
      <Cardlist
        goods={goodsInBasket}
        showModal={showModal}
        closeModal={closeModal}
        isBtnDisabled={isBtnDisabled}
        basketPage={basketPage}
        text={"Delete from card?"}
      />
      {goodsInBasket.length === 0 && (
        <h1 className="empty-string">Your shopping cart is empty:(</h1>
      )}
    </>
  );
};
Basket.propTypes = {
  goodsInBasket: PropTypes.array,
};
export default Basket;
