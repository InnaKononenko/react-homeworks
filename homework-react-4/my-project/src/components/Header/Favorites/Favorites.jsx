import React from "react";
import PropTypes from "prop-types";
import Cardlist from "../../CardsList/CardList";
import ComeBackPage from "../../ComeBackPage";
import { useSelector } from "react-redux";

import { selectorGoodsFavorit } from "../../../store/selectors";
const Favorites = (props) => {
  const { showModal, closeModal } = props;

  const goodsFavorit = useSelector(selectorGoodsFavorit);

  return (
    <>
      <ComeBackPage title="back to shopping" />
      <Cardlist
        goods={goodsFavorit}
        showModal={showModal}
        closeModal={closeModal}
        text={"Add to card?"}
      />
      {goodsFavorit.length === 0 && (
        <h1 className="empty-string">You haven't liked anything yet:(</h1>
      )}
    </>
  );
};
Favorites.propTypes = {
  goodsFavorit: PropTypes.array,
};
export default Favorites;
