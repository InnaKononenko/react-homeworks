import React from "react";
import PropTypes from "prop-types";
import "./buttons.scss";

const Buttons = ({ backgroundColor, text, onClick, isBtnDisabled = false }) => {
  return (
    <button
      className="button"
      type="button"
      onClick={onClick}
      style={{
        backgroundColor: backgroundColor,
        display: isBtnDisabled && "none",
      }}
    >
      {text}
    </button>
  );
};

Buttons.propTypes = {
  backgroundColor: PropTypes.string,
  text: PropTypes.string,
  onClick: PropTypes.func,
};
export default Buttons;
