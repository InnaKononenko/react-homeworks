import React from "react";

import "./Footer.scss";

const Footer = () => {
  return (
    <footer className="footer">
      <div className="copywriting">&#xa9; 2022 Shopogolic.ua</div>
      <div>
        Fashionable clothes, shoes, accessories at an affordable price. All
        rights reserved.
      </div>
    </footer>
  );
};

export default Footer;
