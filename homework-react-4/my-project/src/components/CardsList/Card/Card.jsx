import React from "react";
import PropTypes from "prop-types";
import Button from "../../Buttons";
import Modal from "../../Modal";
import SecondaryButton from "../../SecondaryButton";
import FavoritesCard from "./FavoritesCard";
import { useDispatch, useSelector } from "react-redux";

import {
  addGoodsInBasket,
  deleteGoodsFromBasket,
  setModal,
} from "../../../store/actions";

import { selectorGoodsBasket, selectorModal } from "../../../store/selectors";

const Card = (props) => {
  const dispatch = useDispatch();
  const goodsInBasket = useSelector(selectorGoodsBasket);
  const modal = useSelector(selectorModal);

  const {
    selected,
    selectedCard,
    item,
    showModal,
    closeModal,
    text,
    isBtnDisabled,
    basketPage = false,
  } = props;

  const { show, header, closeButton } = modal;
  const { name, price, url, article, color } = item;

  const addProductToBasket = (product) => {
    const arrArticle = [];
    goodsInBasket.map((el) => {
      arrArticle.push(el.article);
    });
    if (!arrArticle.includes(product.article)) {
      dispatch(addGoodsInBasket(product));
    }
    dispatch(setModal(false));
  };

  const renderButton = (product) => {
    const arrArticle = [];
    goodsInBasket.map((el) => {
      arrArticle.push(el.article);
    });
    if (!arrArticle.includes(product.article)) {
      return (
        <Button
          key={article}
          backgroundColor={"rgb(247, 228, 247)"}
          text={"Add to card"}
          isBtnDisabled={isBtnDisabled}
          onClick={() => {
            showModal();
            selectedCard(item);
          }}
        />
      );
    } else {
      return <Button key={article} text={"✓ Added"} />;
    }
  };
  const deleteFromBasket = (product) => {
    dispatch(
      deleteGoodsFromBasket(
        goodsInBasket.filter((el) => el.article !== product.article)
      )
    );
    dispatch(setModal(false));
  };

  return (
    <div className="card">
      <FavoritesCard className="card-favorites" item={item} />
      {basketPage && (
        <button
          type="button"
          className="delete-card"
          onClick={() => {
            showModal();
            selectedCard(item);
          }}
        >
          <span>&times;</span>
        </button>
      )}
      <div className="card-body">
        <div className="card-header">
          <img className="card-img" src={url} alt="goods" />
          <p className="card-article">{article}</p>
        </div>
        <h5 className="card-name">
          {name.toUpperCase()} {color}
        </h5>
        <h3 className="card-price">{price}</h3>
      </div>
      <div className="button-container">{renderButton(item)}</div>
      {show && (
        <Modal
          show={show}
          closeModal={closeModal}
          closeButton={closeButton}
          header={header}
          text={text}
          actions={
            <div className="btn-footer">
              {!basketPage && (
                <SecondaryButton
                  text={"ok"}
                  func={() => {
                    addProductToBasket(selected);
                  }}
                />
              )}

              {basketPage && (
                <SecondaryButton
                  text={"ok"}
                  func={() => {
                    deleteFromBasket(selected);
                  }}
                />
              )}
              <SecondaryButton text={"no"} func={() => closeModal()} />
            </div>
          }
        />
      )}
    </div>
  );
};

Card.defaultProps = {
  article: "1010",
  color: "to choose",
};
Card.propTypes = {
  selected: PropTypes.object,
  item: PropTypes.object,
  goods: PropTypes.array,
  showModal: PropTypes.func,
  closeModal: PropTypes.func,
  selectedCard: PropTypes.func,
};
export default Card;
