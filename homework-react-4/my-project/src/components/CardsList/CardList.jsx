import React, { useState } from "react";
import PropTypes from "prop-types";
import Card from "./Card";
import "./cardList.scss";

const Cardlist = (props) => {
  const { goods, showModal, closeModal, isBtnDisabled, basketPage, text } =
    props;
  const [selected, setSelected] = useState({});

  const selectedCard = (item) => {
    setSelected(item);
  };

  return (
    <div className="card-list">
      {goods.length > 0 &&
        goods.map((item, key) => {
          return (
            <Card
              selected={selected}
              selectedCard={selectedCard}
              key={key}
              item={item}
              goods={goods}
              showModal={showModal}
              closeModal={closeModal}
              isBtnDisabled={isBtnDisabled}
              basketPage={basketPage}
              text={text}
            />
          );
        })}
    </div>
  );
};

Cardlist.prorTypes = {
  selected: PropTypes.object,
  selectedCard: PropTypes.func,
  text: PropTypes.string,
  goods: PropTypes.array,
  button: PropTypes.object,
  showModal: PropTypes.func,
  closeModal: PropTypes.func,
};
export default Cardlist;
