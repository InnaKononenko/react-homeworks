import React, { useEffect } from "react";
import { Route, Routes } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";

import {
  fetchGoodsList,
  addGoodsBasketInLokal,
  addGoodsFavoritInLokal,
  setModal,
} from "../src/store/actions";
import {
  selectorGoods,
  selectorGoodsBasket,
  selectorGoodsFavorit,
} from "../src/store/selectors";
import "./App.css";
import Cardlist from "./components/CardsList/CardList";
import Header from "./components/Header/Header";
import Basket from "./components/Header/Basket";
import Favorites from "./components/Header/Favorites";
import Footer from "./components/Footer";

const App = () => {
  const dispatch = useDispatch();
  const goods = useSelector(selectorGoods);
  const goodsInBasket = useSelector(selectorGoodsBasket);
  const goodsFavorit = useSelector(selectorGoodsFavorit);

  useEffect(() => {
    dispatch(fetchGoodsList());
  }, []);

  useEffect(() => {
    if (localStorage.getItem("goodsInBasket")) {
      const parseBasket = JSON.parse(localStorage.getItem("goodsInBasket"));
      dispatch(addGoodsBasketInLokal(parseBasket));
    } else {
      localStorage.setItem("goodsInBasket", JSON.stringify(goodsInBasket));
    }

    if (localStorage.getItem("goodsFavorit")) {
      const parseFavorite = JSON.parse(localStorage.getItem("goodsFavorit"));
      dispatch(addGoodsFavoritInLokal(parseFavorite));
    } else {
      localStorage.setItem("goodsFavorit", JSON.stringify(goodsFavorit));
    }
  }, []);

  useEffect(() => {
    localStorage.setItem("goodsInBasket", JSON.stringify(goodsInBasket));
    localStorage.setItem("goodsFavorit", JSON.stringify(goodsFavorit));
  }, [goodsInBasket, goodsFavorit]);

  const showModal = () => {
    dispatch(setModal(true));
  };

  const closeModal = () => {
    dispatch(setModal(false));
  };

  return (
    <>
      <Header showModal={showModal} closeModal={closeModal} />
      <div className="container">
        <Routes>
          <Route
            path="/basket"
            element={
              <Basket
                isBtnDisabled={true}
                goods={goods}
                showModal={showModal}
                closeModal={closeModal}
                basketPage={true}
              />
            }
          />
          <Route
            path="/favorites"
            element={
              <Favorites
                isBtnDisabled={true}
                goods={goods}
                showModal={showModal}
                closeModal={closeModal}
              />
            }
          />
          <Route
            path="/"
            element={
              <Cardlist
                text={"Add to card?"}
                goods={goods}
                showModal={showModal}
                closeModal={closeModal}
              />
            }
          />
        </Routes>
      </div>
      <Footer />
    </>
  );
};
export default App;
