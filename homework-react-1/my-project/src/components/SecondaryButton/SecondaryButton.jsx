import React, { Component } from "react";
import PropTypes from "prop-types";

import "./secondaryButton.scss";

export default class SecondaryButton extends Component {
  render() {
    const { text, func } = this.props;

    return (
      <>
        <button className="btn-text" type="button" onClick={func}>
          {text}
        </button>
      </>
    );
  }
}
SecondaryButton.propTypes = {
  text: PropTypes.string,
  func: PropTypes.func,
};
