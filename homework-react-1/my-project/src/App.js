import React, { Component } from "react";
import Button from "./components/Buttons";
import Modal from "./components/Modal";
import SecondaryButton from "./components/SecondaryButton";

import "./App.css";

class App extends Component {
  state = {
    firstButton: {
      backgroundColor: "rgb(198, 141, 214)",
      textOpen: "Open first Modal",
    },
    secondButton: {
      backgroundColor: "rgb(125, 239, 245)",
      textOpen: "Open second Modal",
    },
    firstModal: {
      show: false,
      text: "Change color this modal?",
      header: "FirstModal",
      closeButton: true,
      backgroundColor: null,
      actions: (
        <div className="btn-footer">
          <SecondaryButton text={"ok"} func={() => this.changeColor()} />
          <SecondaryButton text={"no"} func={() => this.closeFirstModal()} />
        </div>
      ),
    },
    secondModal: {
      show: false,
      text: "Change text this modal?",
      header: "SecondModal",
      closeButton: false,
      actions: (
        <div className="btn-footer">
          <SecondaryButton text={"ok"} func={() => this.changeText()} />
          <SecondaryButton text={"no"} func={() => this.closeSecondModal()} />
        </div>
      ),
    },
  };
  changeText = () => {
    const { secondModal } = this.state;
    this.setState({
      ...this.state,
      secondModal: { ...secondModal, text: "You change this text!" },
    });
  };
  changeColor = () => {
    const { firstModal } = this.state;
    this.setState({
      ...this.state,
      firstModal: { ...firstModal, backgroundColor: "rgb(198, 141, 214)" },
    });
  };
  showFirstModal = () => {
    const { firstModal } = this.state;
    this.setState({ ...this.state, firstModal: { ...firstModal, show: true } });
  };

  showSecondModal = () => {
    const { secondModal } = this.state;
    this.setState({
      ...this.state,
      secondModal: { ...secondModal, show: true },
    });
  };

  closeFirstModal = () => {
    const { firstModal } = this.state;
    this.setState({
      ...this.state,
      firstModal: { ...firstModal, show: false },
    });
  };
  closeSecondModal = () => {
    const { secondModal } = this.state;
    this.setState({
      ...this.state,
      secondModal: { ...secondModal, show: false },
    });
  };

  render() {
    const { firstButton, secondButton, firstModal, secondModal } = this.state;
    const { backgroundColor: bgcFirst, textOpen: textFirst } = firstButton;
    const { backgroundColor: bgcSecond, textOpen: textSecond } = secondButton;
    const {
      show: showFirst,
      header: headFirst,
      text: textFirt,
      closeButton: closeFirst,
      actions: actionsFirst,
      backgroundColor,
    } = firstModal;
    const {
      show: showSecond,
      header: headSecond,
      text: texSecond,
      closeButton: closeSecond,
      actions: actionsSecond,
    } = secondModal;

    return (
      <>
        <div className="bg"></div>
        <div className="section">
          <div className="button-container">
            <Button
              backgroundColor={bgcFirst}
              text={textFirst}
              onClick={this.showFirstModal}
            />
            <Button
              backgroundColor={bgcSecond}
              text={textSecond}
              onClick={this.showSecondModal}
            />
          </div>
        </div>{" "}
        {showFirst && (
          <Modal
            show={showFirst}
            closeModal={this.closeFirstModal}
            closeButton={closeFirst}
            header={headFirst}
            text={textFirt}
            actions={actionsFirst}
            backgroundColor={backgroundColor}
          />
        )}
        {showSecond && (
          <Modal
            show={showSecond}
            closeModal={this.closeSecondModal}
            closeButton={closeSecond}
            header={headSecond}
            text={texSecond}
            actions={actionsSecond}
          />
        )}
      </>
    );
  }
}

export default App;
